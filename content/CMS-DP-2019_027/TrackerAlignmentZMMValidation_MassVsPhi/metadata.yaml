title: '$Z^{0}$ peak mass as a function of the azimuthal angle of the positive/negative muon $\phi(\mu^{+})$ ($\phi(\mu^{-})$) for all events at all psedorapidities'
caption: 'Distortions of the tracker geometry can lead to a bias in the reconstructed track curvature $\kappa \propto \pm\frac{1}{p_{T}}$. These are investigated using the reconstructed Z$\rightarrow\mu\mu$ mass as a function of the muon direction and separating $\mu^{+}$ and $\mu^{-}$ as the curvature bias has an opposite effect on their $p_{T}$. The invariant mass distribution is fitted to a Breit-Wigner convolved with a Crystal Ball function, thus taking into account the finite track resolution and the radiative tail, for the signal plus an exponential background. The fit range is 75-105 GeV/$c^{2}$ and the $Z^{0}$ width is fixed to the PDG value of 2.495 GeV/$c^{2}$ [1]. Note that this does not show the CMS muon reconstruction and calibration performance. Additional muon momentum calibrations are applied on top of this in physics analyses. 

$Z^{0}$ peak mass as a function of the azimuthal angle of the positive muon $\phi(\mu^{+})$ for all events at all psedorapidities. This distribution is sensitive to distortions of the tracker in the transverse plane (e.g. the so-called "sagitta"). The black points show the results with the alignment constants used in End-Of-Year (EOY) 2017 processing, the red points show the results with the alignment constants as obtained in the Run-2 Legacy alignment procedure. Overall pattern in the Legacy processing is significantly reduced with respect to End-Of-Year reprocessing 2017 data.

[1] CMS Collaboration "Alignment of the CMS tracker with LHC and cosmic ray data" 2014 JINST 9 P06009 doi:10.1088/1748-0221/9/06/P06009'
date: '2019-09-17'
tags:
- Run-2
- Collisions
- Phase-1 Pixel
- Tracker
- Alignment
- pp
- 2017