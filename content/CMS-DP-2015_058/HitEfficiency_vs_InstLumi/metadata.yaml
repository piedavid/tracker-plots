title: 'Hit Efficiency as a Function of Instantaneous Luminosity'
caption: 'Hit efficiency measurement 

  $\bullet$ Hit efficiency is the probability to find any clusters within a 500 $\mu$m area around an expected hit:
 
   – The goal is to quantify pure sensor efficiency with tracking effects maximally removed; 

   – Temporary efficiency loss is observed in function of hit-rate (dynamic inefficiency most dominantly due to buffer overflow); largest effect is visible in the innermost layer (Layer 1).   
 
  $\bullet$  Expected hits are provided by good quality tracks:
 
   - Associated to primary vertex with small impact parameter; 

   – With p$_{T}$ > 1.0 GeV; 

   – Missing hit is allowed only on layer under investigation (valid hits are expected on two „other” layers/disks).

 $\bullet$  Module selection: 

   – Bad read-outs removed; 

   – Read-out chips (ROC) under SEU (temporarily unfunctional ROCs) removed; 

   – ROC and module edges, as well as, overlap areas of adjacent modules within a layer rejected;

   – Only modules with good illumination by tracks are selected; 

 $\bullet$  Error bars are dominated by systematic uncertainty estimated to be ~0.3%.

The plot shows efficiency loss as function of instantaneous luminosity: 

 $\bullet$ The statistical error in the measurement is shown, systematic uncertainty of 0.3% is not shown; 

 $\bullet$ Higher inst. lumi results in higher hit-rate therefore larger inefficiency; 

 $\bullet$ Trend vs instantaneous luminosity depends on the LHC filling scheme; here we see the net result of various filling schemes the LHC had over 2015;

 $\bullet$ Maximum efficiency loss is much less than measured in 2012.'
date: '2015/12/15'
tags:
- Run-2
- Phase-0 Pixel
- 2015
- pp  
- Collisions
